import csv
import mrf3fundef


def itemlist(fname):
    columns = []
    i = 0
    with open(fname, newline='') as csvfile:
        lines = csv.reader(csvfile, delimiter='\t', quotechar='|')
        for line in lines:
            if len(line) > 10 and line[1] == 'FVaL':
                for item in line:
                    if item != '':
                        columns += [i]
                    i += 1
                break
    return columns


def mrf1list(fname):
    columns = itemlist(fname)
    fifo = [['Material', 'Material description', 'Stock', 'Unit', 'Value', 'Crcy', 'FIFO', 'Deval', 'Deval %', 'Price',
             'per', 'Type']]
    i = 0
    with open(fname, newline='') as csvfile:
        lines = csv.reader(csvfile, delimiter='\t', quotechar='|')
        for line in lines:
            if len(line) > 20 and line[1] == 'FVaL':
                i = 0
            if len(line) > 18 and i > 2:
                material = line[columns[1]]
                materialdesc = line[columns[2]]
                stock = line[columns[3]].strip().replace('.', '').replace(' ', '')
                unit = line[columns[4]]
                value = line[columns[5]].strip().replace('.', '').replace(' ', '')
                crcy = line[columns[6]]
                fifovalue = line[columns[7]].strip().replace('.', '').replace(' ', '')
                devaluation = line[columns[8]].strip().replace('.', '').replace(' ', '')
                devaluationpercent = line[columns[10]].strip().replace('.', '').replace(' ', '')
                price = line[columns[12]].strip().replace('.', '').replace(' ', '')
                per = line[columns[14]].strip().replace('.', '').replace(' ', '')
                type = line[columns[14] + 1]
                fifo += [[material, materialdesc, stock, unit, value, crcy, fifovalue, devaluation, devaluationpercent,
                          price, per, type]]
            i += 1
    return fifo


def matstock(fifo):
    fifodict = dict()
    f = 1
    for item in range(len(fifo) - 1):
        stock = float(fifo[f][2].replace(',', '.').replace(' ', ''))
        fifodict[fifo[f][0]] = stock
        f += 1
    return fifodict


def materialqty(fifodict, material):
    try:
        result = fifodict[material]
    except KeyError:
        result = 0
    return result