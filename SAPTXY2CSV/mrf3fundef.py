import csv
import fundef
import mrf1fundef

def addirl(gr, irl):
    i = 0
    for i in range(len(irl)):
        if gr[3:7] == irl[i][3:7]:
            gr[7] += irl[i][7]
            irl.remove(irl[i])
            break
    return gr

def mrf3list(fname):
    purchases = [['Sh. Text', 'Type', 'MvT', 'Order', 'Item', 'Date', 'Material', 'Value', 'Crcy', 'Qty', 'Unit']]
    irl = [['Sh. Text', 'Type', 'MvT', 'Order', 'Item', 'Date', 'Material', 'Value', 'Crcy', 'Qty', 'Unit']]
    with open(fname, newline='') as csvfile:
        lines = csv.reader(csvfile, delimiter='\t', quotechar='|')
        for line in lines:
            if len(line) > 1 and line[1] == 'Indiv. Trans.':
                if line[4] != 'NeuR':
                    shtext = line[4]
                    type = line[7]
                    mvt = line[9]
                    order = line[14]
                    orderitm = line[15].strip()
                    pstngdate = line[16]
                    material = line[17]
                    receiptvalue = float(line[19].replace('.', '').replace(',', '.').strip().replace(' ', ''))
                    crcy = line[20]
                    if line[4] == 'IR-L' or line[4] == 'SD-L':
                        qty = ''
                        unit = ''
                        irlline = [shtext, type, mvt, order, orderitm, pstngdate, material, receiptvalue, crcy, qty, unit]
                        irlline = addirl(irlline, irl)
                        irl += [irlline]
                    else:
                        qty = float(line[21].replace('.', '').replace(',', '.').strip().replace(' ', ''))
                        unit = line[22]
                        row = [shtext, type, mvt, order, orderitm, pstngdate, material, receiptvalue, crcy, qty, unit]
                        row = addirl(row, irl)
                        purchases += [row]
        if len(irl) > 1:
            fundef.makecsv('irl.csv', irl)
        return purchases


def matpurchlist(purchases):
    mat = ''
    m = 2
    n = -1
    purchlist = []
    for m in range(len(purchases)):
        if purchases[m][6] != purchases[m - 1][6]:
            purchlist += [[purchases[m][6], [purchases[m][7], purchases[m][9]]]]
            n += 1
        else:
            purchlist[n] += [[purchases[m][7], purchases[m][9]]]
        m += 1

    purchlist.remove(purchlist[0])
    return purchlist

def purchasesprice(matpurchlist):
    qty = 0.0
    value = 0.0
    for i in range(1, len(matpurchlist)):
        qty += matpurchlist[i][1]
        value += matpurchlist[i][0]
    try:
        avg = value / qty
    except ZeroDivisionError:
            avg = 0
    return avg

def purchasespricewozero(matpurchlist):
    qty = 0.0
    value = 0.0
    for i in range(1, len(matpurchlist)):
        if matpurchlist[i][0] != 0:
            qty += matpurchlist[i][1]
            value += matpurchlist[i][0]
        try:
            avg = value / qty
        except ZeroDivisionError:
            avg = 0.0
    return avg

def firstlastpoprice(matpurchlist, firstlast):
    qty = 0.0
    value = 0.0
    qty = matpurchlist[firstlast][1]
    value = matpurchlist[firstlast][0]
    try:
        avg = value / qty
    except ZeroDivisionError:
            avg = 0
    return avg

def minmaxprice(matpurchlist, mm):
    minmax = []
    for i in range(1, len(matpurchlist)):
        qty = matpurchlist[i][1]
        value = matpurchlist[i][0]
        try:
            minmax += [value / qty]
        except ZeroDivisionError:
            minmax += []
    if mm == 'min':
        result = min(minmax)
    elif mm == 'max':
        result = max(minmax)
    return result

def fifoprice(matpurchlist, stock):
    value = 0.0
    origstock = stock
    avg = 0.0
    for i in range(1, len(matpurchlist)):
        itemvalue = matpurchlist[i][0]
        itemqty = matpurchlist[i][1]
        if stock >= itemqty:
            value += itemvalue
            stock += -itemqty
        elif stock < itemqty:
            value += stock * (itemvalue / itemqty)
            break
        else:
            break
    try:
        avg = value / origstock
    except ZeroDivisionError:
        avg = purchasespricewozero(matpurchlist)
    return avg

def fifopricewozero(matpurchlist, stock):
    value = 0.0
    origstock = stock
    avg = 0.0
    for i in range(1, len(matpurchlist)):
        itemvalue = matpurchlist[i][0]
        itemqty = matpurchlist[i][1]
        if stock >= itemqty and itemvalue != 0:
            value += itemvalue
            stock += -itemqty
        elif stock >= itemqty and itemvalue == 0:
            stock += -itemqty
            origstock += -itemqty
        elif stock < itemqty and itemvalue != 0:
            value += stock * (itemvalue / itemqty)
            break
        else:
            break
    try:
        avg = value / origstock
    except ZeroDivisionError:
        avg = 0.0
    return avg

def fifodata(purchlist, fifo):
    materialstock = mrf1fundef.matstock(fifo)
    purchdata = [['Material', 'Avg', 'Avg wo 0', 'First', 'Last', 'Min', 'Max', 'FIFO', 'FIFO wo zero']]
    for item in purchlist:
        material = item[0]
        avgpurchprice = purchasesprice(item)
        avgpurchpricewozero = purchasespricewozero(item)
        lastprice = firstlastpoprice(item, 1)
        firstprice = firstlastpoprice(item, -1)
        minprice = minmaxprice(item, 'min')
        maxprice = minmaxprice(item, 'max')
        fifo = fifoprice(item, mrf1fundef.materialqty(materialstock, material))
        fifowozero = fifopricewozero(item, mrf1fundef.materialqty(materialstock, material))
        purchdata += [[material, avgpurchprice, avgpurchpricewozero, firstprice, lastprice, minprice, maxprice, fifo, fifowozero]]
    return purchdata